from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.
def say_hello(request):
    return HttpResponse("Hello, World!")
import random
# Create your views here.

R_P_S = ["rock", "paper", "scissors"]

def computer_guess() -> str:
    return random.choice(R_P_S)

def play_rock_paper_scissors(p1: str, p2: str) -> str:
    if p1.lower() == p2.lower():
        return "draw"
    outcomes = {("rock", "paper"): "paper",
                ("scissors", "paper"): "scissors",
                ("scissors", "rock"): "rock"}
    inputs = (max(p1, p2), min(p1, p2))
    return outcomes[inputs]

def say_hello(request):
    return HttpResponse('''
    <!doctype html>
    <html>
        <body>
            <h3>Greetings</h3>
            <p>Hello, World!</p>
        </body>
    </html>''')

def play_rps(request, user_choice):
    #user_choice = request.GET['user_choice']
    #user_choice = request.GET.get('user_choice', 'rock')
    computer_choice = computer_guess()
    outcome = play_rock_paper_scissors(user_choice, computer_choice)
    return HttpResponse(f'''
    <!doctype html>
    <html>
        <body>
            <h3>Rock-Paper-Scissors</h3>
            <p>You Chose {user_choice}</p>
            <p>Computer chose {computer_choice}</p>
            <p>Winner: {outcome}</p>
        </body>
    </html>
    ''')
def show_rps_form(request):
    return render(request, "hello/rps_form.html")

def process_rps_form(request):
    user_choice = request.POST['user_choice']
    computer_choice = computer_guess()
    outcome = play_rock_paper_scissors(user_choice, computer_choice)
    context = {"user": user_choice,
               "computer": computer_choice,
               "outcome": outcome}
    return render(request, "hello/rps.html", context)

import random
from django.shortcuts import render

questions = {
    
    "Who was the first Prime Minister of India?": "Jawaharlal Nehru",
    "What is the capital of India?": "Delhi",
    "Name the deepest ocean in the world?": "Pacific Ocean",
    "What is the largest planet in our solar system?": "Jupiter",
    "What is the square root of 144?": "12"
}


def quiz_view(request):
    question = random.choice(list(questions.keys())) 
    context = {'question': question}
    return render(request, 'hello/quiz.html', context)

def check_answer_view(request):
    if request.method == 'POST':
        user_answer = request.POST.get('answer')
        question = request.POST.get('question')
        correct_ans = questions.get(question)
        if user_answer == correct_ans:
            result = "Correct answer!"
        else:
            result = "Incorrect answer!"
        context = {'result': result,
                   'user_ans': user_answer,
                   'correct': questions[question]
                       }
        return render(request, 'hello/reason.html', context)
    
    
    #genearation of words
from django.shortcuts import render
import random
word_list = ['adinarayana', 'zoom', 'ox', 'dog', 'elephant', 'flamingo', 'snake']
def home(request):
    return render(request, 'hello/home.html')

def play_game(request):
    return render(request, 'hello/play_game.html')

def generate_word(num_letters, starting_letter, ignore_letter):

    filtered_list = [word for word in word_list if num_letters < len(word)and  starting_letter  == word[0] and ignore_letter not in word_list]
    if filtered_list:
        return filtered_list
    else:
        return None

def check_answer(request):
    if request.method == 'POST':
        num_letters = int(request.POST.get('num_letters'))
        starting_letter = request.POST.get('starting_letter').lower()
        ignore_letter = request.POST.get('ignore_letter').lower()

        word1 = generate_word(num_letters, starting_letter, ignore_letter)
        if word1:
            result = f"The word is: {word1}"
        else:
            result = "No word found with the specified conditions."
        
        context = {'result': result}
        return render(request, 'hello/check_answer.html', context)