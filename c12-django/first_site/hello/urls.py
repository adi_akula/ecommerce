from django.urls import path
from . import views

urlpatterns = [
    path('sayhello', views.say_hello, name='say_hello'),
    path('rps/direct/<str:user_choice>', views.play_rps, name='play_rps'),
    path('rps/form', views.show_rps_form, name='show_rps_form'),
    path('rps/processform', views.process_rps_form, name='process_rps_form'),
    path('quiz', views.quiz_view, name='quiz'),
    path('check-answer/', views.check_answer_view, name='check_answer_view'),
     path('game/', views.home, name='home'),
    path('play/', views.play_game, name='play_game'),
    path('check/', views.check_answer, name='check_answer')
    
]