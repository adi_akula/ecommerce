from django.urls import path
from . import views

urlpatterns = [
    path('info/', views.employe_list, name='employe_list'),
    path('add', views.add_employe, name='add_employe'),
    path('detail/<int:id>', views.employe_detail, name='employe_detail'),
    path('delete/<int:id>', views.delete_employe, name='delete_employe'),
    path('edit/<int:id>', views.update_employe, name='update_employe')
]