from django.shortcuts import render, redirect
from . models import employe, Employe_attendance
# Create your views here.

def employe_list(request):
    employee = employe.objects.all()
    return render(request, 'office/employe_list.html', 
                  {'employee': employee})
    
    
def add_employe(request):
    if request.method == 'GET':
        return render(request, 'office/add_employe.html')
    else:
        name_ = request.POST['name']
        employe_id_ = request.POST['employe_id']
        salary_ = request.POST['salary']

        e = employe(
            name = name_,
            employe_id = employe_id_,
            salary = salary_
        )
        e.save()
        return redirect('employe_list')
    
#READ    
def employe_detail(request, id):
    e = employe.objects.get(id=id)
    a = Employe_attendance.objects.filter(emp = e)
    return render(request, 'office/employe_detail.html', {'employe': e, 'a':a})

#DELETE
def delete_employe(request, id):
    e = employe.objects.get(id=id)
    e.delete()
    return redirect('employe_list')

#UPDATE
def update_employe(request, id):
    if request.method == 'GET':
        e = employe.objects.get(id=id)
        editing = True
        context = {'employe': e, 'editing': editing}
        return render(request, 'office/employe_detail.html', context)

    if request.method == 'POST':
        e = employe.objects.get(id=id)
        e.name = request.POST['name']
        e.salary = request.POST['salary']
        e.employe_id = request.POST['employe_id']
        e.save()
        return redirect('employe_detail', id)

#C for CREATE    
def add_attendance(request, id):
    e = employe.objects.get(id=id)
    new_e_date = request.POST['e_date']
    new_e_attendance = request.POST['e_attendance']
    e = Employe_attendance.objects.create(\
            employe = e,
            e_date = new_e_date,
            e_attendance = new_e_attendance
        )
    return redirect('employe_detail', id)

#U for UPDATE and D for DELETE and R fo READ
def edit_attendance(request, id, attendance_id):

    m = Employe_attendance.objects.get(id=attendance_id)
    if request.method == 'GET':
        return render(request, 'office/attendance.html', {'attend': m})
    elif request.method == 'POST':
        if request.POST.get('edit') == 'EDIT':
            m.e_date = request.POST['e_date']
            m.e_attendance = request.POST['e_attendance']
            m.save()
            return redirect('edit_attendance', id, attendance_id)
        else:
            m.delete()
            return redirect('employe_detail', id)
