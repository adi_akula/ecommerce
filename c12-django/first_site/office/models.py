from django.db import models
from django import forms

# Create your models here.
class employe(models.Model):
    name = models.CharField(max_length = 100)
    employe_id = models.CharField(max_length = 10)
    salary = models.CharField(max_length = 8)

    def __str__(self):
        return f'{self.name} - {self.employe_id} - {self.salary}'
    
attendance = (("yes", "present"), ("no", "absent"))
    
class Employe_attendance(models.Model):
    e_date = models.DateField()
    e_attendance = models.CharField(max_length = 20, choices = attendance)
    emp = models.ForeignKey(employe, on_delete=models.CASCADE)



    def __str__(self):
        
        return f'(Roll){self.emp.employe_id} - {self.e_date} - {self.e_attendance}'
