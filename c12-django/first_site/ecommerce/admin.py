from django.contrib import admin
from .models import Customer, Order, Merchant
admin.site.register([Customer, Order, Merchant])
# Register your models here.
