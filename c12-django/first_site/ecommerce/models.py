from django.db import models
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.forms import ModelForm
# Create your models here.
class Customer(models.Model):
    customer_name = models.CharField(max_length=100)
    email = models.EmailField()
    address = models.CharField(max_length=100)
    user_name = models.CharField(max_length=100)
    customer_id = models.CharField(max_length=10)
    
    def _str_(self):
        return f'{self.customer_name} - {self.email} - {self.address} - {self.user_name} - {self.customer_id}'


class Merchant(models.Model):
    seller_name = models.CharField(max_length=100)
    product_name = models.CharField(max_length=100)
    description = models.CharField(max_length=400)
    product_id = models.CharField(max_length=10)
    def _str_(self):
        return f'(ids){self.seller_name} - {self.product_name} - {self.product_id} - {self.description}'
    
class Order(models.Model):
    pro_id = models.ForeignKey(Merchant, on_delete=models.CASCADE)
    cus_id = models.ForeignKey(Customer, on_delete=models.CASCADE)
    quantity = models.IntegerField()
    date_ordered = models.DateTimeField(auto_now_add=True)
    status = models.CharField(max_length=20)
    def _str_(self):
        return f'(ids){self.pro_id.product_id} - {self.cus_id.customer_id} - {self.date_ordered} - {self.quantity} {self.status}'
    
    
    